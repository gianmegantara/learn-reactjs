import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Home from './App/MainComponent/App';
import About from './App/MainComponent/About'
import User from './App/MainComponent/ContainerUserProfile'
import FormRegister from './App/MainComponent/FormRegister'
import FormLogin from './App/MainComponent/FormLogin'
import Books from './App/MainComponent/Books'
import Navigation from './App/NavComponent/Navigation'
import GithubRepos from './App/MainComponent/BookComponent(Class)/searchBook/components/searchForm'
import {
    Route, BrowserRouter as Router
} from 'react-router-dom'
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';


const routing = (
    <Router>
        <Navigation />
        <Route exact path='/' component={Home} />
        <Route path='/home' component={Home} />
        <Route path='/about' component={About} />
        <Route path='/user' component={User} />
        <Route path='/register' component={FormRegister} />
        <Route path='/login' component={FormLogin} />
        <Route path='/books' component={Books} />
        <Route path='/iseng' component={GithubRepos} />

    </Router >
)

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
