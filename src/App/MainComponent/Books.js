import React from 'react'
import axios from 'axios'
import FormBook from './BookComponent(Class)/formBook'
import ListBook from './BookComponent(Class)/listBook'
import { Row } from 'reactstrap'


class Books extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            books: [],
            values: {
                editMode: false
            }, valuesError: {
                title: '', author: '', publishedDate: '', pages: '', language: '', publisherId: ''
            }, form: {
                buttonDisabled: true
            }
        }
    }

    componentDidMount = () => {
        axios.get('http://localhost:8002/books').then(res => {
            const books = res.data;
            this.setState({ books: books.data });
        });
    }

    validate = () => {
        let errorsValueState = {}
        let stateData = this.state.values
        let { title, author, publishedDate, pages, language, publisherId } = false

        if (!stateData.title) {
            errorsValueState.title = "title field is required"
            title = false
        } else if (stateData.title.length <= 2) {
            errorsValueState.title = "title field is too short"
            title = false
        } else {
            title = true
        }

        if (!stateData.author) {
            errorsValueState.author = "author field is required"
            author = false
        } else if (stateData.author.length <= 2) {
            errorsValueState.author = "author field is too short"
            author = false
        } else {
            author = true
        }

        if (!stateData.publishedDate) {
            errorsValueState.publishedDate = "date published field is required"
            publishedDate = false
        } else {
            publishedDate = true
        }

        if (!stateData.pages) {
            errorsValueState.pages = "pages field is required"
            pages = false
        } else if (stateData.pages <= 0) {
            errorsValueState.pages = "pages should more than one"
            pages = false
        } else {
            pages = true
        }

        if (!stateData.language) {
            errorsValueState.language = "language field is required"
            language = false
        } else if (stateData.language.length <= 2) {
            errorsValueState.language = "language is too short"
            language = false
        } else {
            language = true
        }

        if (!stateData.publisherId) {
            errorsValueState.publisherId = "publisher name field is required"
            publisherId = false
        } else if (stateData.publisherId.length <= 2) {
            errorsValueState.publisherId = "publisher is too short"
            publisherId = false
        } else {
            publisherId = true
        }

        if (title && author && publishedDate && pages && language && publisherId === true) {
            this.setState({
                form: {
                    buttonDisabled: false
                }
            })
        } else {
            this.setState({
                form: {
                    buttonDisabled: true
                }
            })
        }

        return errorsValueState
    }

    handleChange = (event) => {
        const { name, value } = event.target
        this.setState({
            values: { ...this.state.values, [name]: value }
        }, () => {
            this.setState({
                valuesError: this.validate()
            })
        })
    }

    handleSubmit = (event) => {
        event.preventDefault()
        const data = {
            title: this.state.values.title,
            author: this.state.values.author,
            publishedDate: this.state.values.publishedDate,
            pages: this.state.values.pages,
            language: this.state.values.language,
            publisherId: this.state.values.publisherId
        }
        axios.post(`http://localhost:8002/books`, data)
            .then((res) => {
                if (res.status === 201) {
                    this.setState({
                        values: {
                            id: '',
                            title: '',
                            author: '',
                            publishedDate: '',
                            pages: '',
                            language: '',
                            publisherId: ''
                        },
                        form: {
                            buttonDisabled: true
                        }
                    })
                }
                this.componentDidMount()
            })
            .catch(err => {
                console.log(err)
            })
    }

    handleEdit = bookId => {
        axios.get(`http://localhost:8002/books/${bookId}`)
            .then(result => {
                this.setState({
                    values: {
                        id: result.data.data.id,
                        title: result.data.data.title,
                        author: result.data.data.author,
                        publishedDate: result.data.data.publishedDate,
                        pages: result.data.data.pages,
                        language: result.data.data.language,
                        publisherId: result.data.data.publisherId,
                        editMode: true
                    }
                })
            })
            .catch(err => {
                console.log(err)
            })
    }

    handleEditBook = (event) => {
        event.preventDefault()
        const id = this.state.values.id
        const data = {
            title: this.state.values.title,
            author: this.state.values.author,
            publishedDate: this.state.values.publishedDate,
            pages: this.state.values.pages,
            language: this.state.values.language,
            publisherId: this.state.values.publisherId
        }
        axios.put(`http://localhost:8002/books/${id}`, data)
            .then(res => {
                if (res.status === 200) {
                    this.setState({
                        values: {
                            id: '',
                            title: '',
                            author: '',
                            publishedDate: '',
                            pages: '',
                            language: '',
                            publisherId: '',
                            editMode: false
                        },
                        form: {
                            buttonDisabled: true
                        }
                    })
                }
                this.componentDidMount()
            })
            .catch(err => {
                console.log(err)
            })
    }

    handleDelete = bookId => {
        axios.delete(`http://localhost:8002/books/${bookId}`)
            .then(result => {
                if (result.status === 201) {
                    this.componentDidMount()
                }
            })
            .catch(err => {
                console.log(err)
            })
    }

    handleBookById = bookId => {
        axios.get(`http://localhost:8002/books/${bookId}`)
            .then(result => {
                if (result.status === 201) {
                    this.setState({
                        values: {
                            id: result.data.data.id,
                            title: result.data.data.title,
                            author: result.data.data.author,
                            publishedDate: result.data.data.publishedDate,
                            pages: result.data.data.pages,
                            language: result.data.data.language,
                            publisherId: result.data.data.publisherId
                        }
                    })
                }
                console.log(result)
            })
            .catch(err => {
                console.log(err)
            })
    }

    render() {
        return (
            <div className='container'>
                <Row>
                    <FormBook state={this.state} handleChange={this.handleChange} handleSubmit={this.handleSubmit} handleEditBook={this.handleEditBook} />
                    <ListBook state={this.state} handleDelete={this.handleDelete} handleEdit={this.handleEdit} handleBookById={this.handleBookById} />
                </Row>
            </div>
        )
    }
}
export default Books