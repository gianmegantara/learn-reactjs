import React from 'react'
import UserProfile from '../UserProfileComponent/UserProfile'
import { Row } from 'reactstrap';

function ContainerUserProfile() {

    return (
        <div className="container">
            <Row>
                <UserProfile />
            </Row>
        </div>

    );
}

export default ContainerUserProfile;
