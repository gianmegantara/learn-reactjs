import React from 'react'
import { Col, Form, FormGroup, Label, Input, Button } from 'reactstrap'


const FormBook = (props) => {
    return (
        <div>
            <Col>
                <Form className="mt-5 mb-5" onSubmit={props.state.values.editMode ? props.handleEditBook : props.handleSubmit}>
                    <h2>Form Book</h2>
                    <FormGroup>
                        <Label for="title">Title</Label>
                        <Input type="text" name="title" id="Title" value={props.state.values.editMode ? props.state.values.title : props.state.values.title} placeholder="Input the title of book" onChange={props.handleChange} />
                        <p>{props.state.valuesError.title}</p>
                    </FormGroup>
                    <FormGroup>
                        <Label for="author">Author</Label>
                        <Input type="text" name="author" id="author" value={props.state.values.editMode ? props.state.values.author : props.state.values.author} placeholder="Input the author of book" onChange={props.handleChange} />
                        <p>{props.state.valuesError.author}</p>
                    </FormGroup>
                    <FormGroup>
                        <Label for="publishedDate">Date Published</Label>
                        <Input type="date" name="publishedDate" id="publishedDate" value={props.state.values.editMode ? props.state.values.publishedDate : props.state.values.publishedDate} placeholder="Choose the date" onChange={props.handleChange} />
                        <p>{props.state.valuesError.publishedDate}</p>
                    </FormGroup>
                    <FormGroup>
                        <Label for="pages">Pages</Label>
                        <Input type="number" name="pages" id="pages" value={props.state.values.editMode ? props.state.values.pages : props.state.values.pages} placeholder="Input the pages of date" onChange={props.handleChange} />
                        <p>{props.state.valuesError.pages}</p>
                    </FormGroup>
                    <FormGroup>
                        <Label for="language">Language</Label>
                        <Input type="text" name="language" id="language" value={props.state.values.editMode ? props.state.values.language : props.state.values.language} placeholder="Input the language of book" onChange={props.handleChange} />
                        <p>{props.state.valuesError.language}</p>
                    </FormGroup>
                    <FormGroup>
                        <Label for="publisherId">Publisher Name</Label>
                        <Input type="text" name="publisherId" id="publisherId" value={props.state.values.editMode ? props.state.values.publisherId : props.state.values.publisherId} placeholder="Input the publisher name" onChange={props.handleChange} />
                        <p>{props.state.valuesError.publisherId}</p>
                    </FormGroup>
                    <Button type="submit" disabled={props.state.form.buttonDisabled}>{props.state.values.editMode ? 'Edit' : 'Submit'}</Button>
                </Form>
            </Col>
        </div>
    )
}
export default FormBook



