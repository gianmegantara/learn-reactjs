import React, { useState } from 'react'
import { Col, Table, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'


const ListBook = (props) => {

    const [modal, setModal] = useState(false)
    const toggle = () => {
        setModal(!modal)
    }


    return (
        <>
            <Col>
                <Table className="mt-5 ml-3">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Author</th>
                            <th>Date Published</th>
                            <th>Pages</th>
                            <th>Language</th>
                            <th>Publisher Name</th>
                            <th colSpan="3" className="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {props.state.books.map(book => (
                            <tr key={book.id}>
                                <td>{book.title}</td>
                                <td>{book.author}</td>
                                <td>{book.publishedDate}</td>
                                <td>{book.pages}</td>
                                <td>{book.language}</td>
                                <td>{book.publisherId}</td>
                                <td><Button onClick={() => { props.handleEdit(book.id) }}>Edit</Button></td>
                                <td><Button onClick={() => { props.handleDelete(book.id) }}>Delete</Button></td>
                                <td><Button onClick={toggle}>Detail</Button></td>
                            </tr>
                        ))}
                    </tbody>
                </Table>
            </Col>
            <div>
                <Modal isOpen={modal} toggle={toggle}>
                    <ModalHeader toggle={toggle}>Detail Book</ModalHeader>
                    <ModalBody>

                    </ModalBody>
                    <ModalFooter>

                        <Button color="secondary" onClick={toggle}>Close</Button>
                    </ModalFooter>
                </Modal>
            </div>
        </>
    )
}
export default ListBook

