import React, { useState } from 'react'
import useGlobal from '../store/store'
import Result from './resultBooks'
import { Form, Input, FormGroup, Label, Button, Col } from 'reactstrap'

const Search = () => {
    const [disable, setDisable] = useState({ disable: true })
    const [globalState, globalActions] = useGlobal()
    const searchSubmit = e => {
        e.preventDefault()
        const title = e.target.title.value
        globalActions.getBooks.getBooksByTitle(title);
        const { books } = globalState
        console.log(books)
    }

    const handleChange = (e) => {
        const value = e.target.value
        console.log(value)
        if (!value.length === 0) {
            setDisable({
                disable: true
            })
        } else {
            setDisable({
                disable: false
            })
        }
    }



    return (
        <div className="container">
            <Col className="col-6 offset-3">
                <Form onSubmit={searchSubmit}>
                    <FormGroup className='ml-5 mt-5 mr-5'>
                        <Label for="Search">Input Title Of Book</Label>
                        <Input type="text" name="title" id="title" placeholder="title book" onChange={handleChange} />
                    </FormGroup>
                    <Button className="ml-5" type='submit' disabled={disable ? true : false}>Search</Button>
                </Form>
            </Col>

            <Result />
        </div>
    )
}

export default Search
