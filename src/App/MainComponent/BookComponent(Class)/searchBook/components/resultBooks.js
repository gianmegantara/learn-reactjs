import React from 'react'
import useGlobal from '../store/store'



const mapBooks = books => {
    return books.map(book => (
        <tr key={book.id}>
            <td>{book.title}</td>
            <td>{book.author}</td>
            <td>{book.publishedDate}</td>
            <td>{book.pages}</td>
            <td>{book.language}</td>
            <td>{book.publisherId}</td>
        </tr>
    ))
}

const Book = () => {
    const [globalState, globalActions] = useGlobal()
    const { books } = globalState
    return (
        <table>
            {mapBooks(books)}
        </table>
    )
}



export default Book
