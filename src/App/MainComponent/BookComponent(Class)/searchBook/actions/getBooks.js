import axios from 'axios'
import useGlobal from '../store/store'

export const getBooksByTitle = async (store, title, request = axios) => {
    try {
        const response = await request.get(`http://localhost:8002/books/title/${title}`)
        const bookis = response.data.data
        console.log(bookis)
        store.setState({ books: bookis })
    } catch (err) {
        console.log(err)
    }
}