import React, { Component } from 'react'
import { FormErrors } from './FormComponent/FormErrors'
import { Row } from 'reactstrap'


class FormRegister extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            email: '',
            password: '',
            confirmationPassword: '',
            formErrors: { name: '', email: '', password: '', confirmationPassword: '' },
            nameValid: false,
            emailValid: false,
            passwordValid: false,
            confirmationPasswordValid: false,
            formValid: false,
            token: ''

        }
    }

    componentDidMount() {
        const tokens = sessionStorage.getItem('token')
        this.setState({
            token: tokens
        })
    }

    handleUserInput = (event) => {
        const name = event.target.name
        const value = event.target.value
        this.setState({
            [name]: value,
        }, () => {
            this.validateField(name, value)
        })
    }

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors
        let nameValid = this.state.nameValid
        let emailValid = this.state.emailValid
        let passwordValid = this.state.passwordValid
        let confirmationPasswordValid = this.state.confirmationPasswordValid

        switch (fieldName) {
            case 'name':
                nameValid = value.length >= 2
                fieldValidationErrors.name = nameValid ? '' : 'name too short'
                break;
            case 'email':
                emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                fieldValidationErrors.email = emailValid ? '' : 'is invalid'
                break;
            case 'password':
                passwordValid = value.length >= 6
                fieldValidationErrors.password = passwordValid ? '' : 'is too short'
                break;
            case 'confirmationPassword':
                confirmationPasswordValid = value === this.state.password
                fieldValidationErrors.confirmationPassword = confirmationPasswordValid ? '' : 'doesnt not match with password'
                break;
            default:
                break;
        }
        this.setState({ formErrors: fieldValidationErrors, emailValid: emailValid, passwordValid: passwordValid }, this.validateForm)
    }
    validateForm() {
        this.setState({ formValid: this.state.nameValid && this.state.emailValid && this.state.passwordValid && this.state.confirmationPasswordValid })
    }

    errorClass = (error) => {
        return (error.length === 0 ? '' : 'has error')
    }

    handleRegister = () => {
        const data = {

        }
    }

    render() {
        return (
            <div className='container'>
                <Row className='m-5'>
                    <form className="demoForm">
                        <h2>Sign up</h2>
                        <div>
                            <FormErrors formErrors={this.state.formErrors} />
                        </div>
                        <div className={`form-group ${this.errorClass(this.state.formErrors.name)}`}>
                            <label htmlFor="name">Name</label>
                            <input type="text" required className="form-control" name="name" placeholder="name" onChange={this.handleUserInput} />
                        </div>
                        <div className={`form-group ${this.errorClass(this.state.formErrors.email)}`}>
                            <label htmlFor="email">Email address</label>
                            <input type="email" required className="form-control" name="email" placeholder="Email" onChange={this.handleUserInput} />
                        </div>
                        <div className={`form-group ${this.errorClass(this.state.formErrors.password)}`}>
                            <label htmlFor="password">Password</label>
                            <input type="password" className="form-control" name="password" placeholder="Password" onChange={this.handleUserInput} />
                        </div>
                        <div className={`form-group ${this.errorClass(this.state.formErrors.confirmationPassword)}`}>
                            <label htmlFor="password">Confirmation Password</label>
                            <input type="password" className="form-control" name="confirmationPassword" placeholder="Confirmation Password" onChange={this.handleUserInput} />
                        </div>
                        <button type="submit" className="btn btn-primary" >Sign Up</button>
                    </form>
                    <p>{this.state.token}</p>
                </Row>
            </div>
        )
    }
}
export default FormRegister