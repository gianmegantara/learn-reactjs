import React, { Component } from 'react'
import { FormErrors } from './FormComponent/FormErrors'
import { Row } from 'reactstrap'
import axios from 'axios'


class FormLogin extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            formErrors: { email: '', password: '' },
            emailValid: false,
            passwordValid: false,
            formValid: false
        }
    }

    handleUserInput = (event) => {
        const name = event.target.name
        const value = event.target.value

        this.setState({
            [name]: value
        }, () => {
            this.validateField(name, value)
        })
    }

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors
        let emailValid = this.state.emailValid
        let passwordValid = this.state.passwordValid

        switch (fieldName) {
            case 'email':
                emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                fieldValidationErrors.email = emailValid ? '' : 'is invalid'
                break;
            case 'password':
                passwordValid = value.length >= 6
                fieldValidationErrors.password = passwordValid ? '' : 'is too short'
                break;
            default:
                break;
        }
        this.setState({ formErrors: fieldValidationErrors, emailValid: emailValid, passwordValid: passwordValid }, this.validateForm)
    }
    validateForm() {
        this.setState({ formValid: this.state.emailValid && this.state.passwordValid })
    }

    errorClass = (error) => {
        return (error.length === 0 ? '' : 'has error')
    }

    handleSignIn = (e) => {
        e.preventDefault()
        const data = {
            username: this.state.email,
            password: this.state.password
        }
        axios.post(`http://localhost:8080/login`, data)
            .then(res => {
                console.log(res.data.accessToken)
                sessionStorage.setItem('token', res.data.accessToken)
            })
            .catch(err => {
                console.log(err)
            })
    }

    render() {
        return (
            <div className='container jumbotron'>
                <Row className='m-5'>
                    <form className="demoForm" onSubmit={this.handleSignIn}>
                        <h2>Sign In</h2>
                        <div>
                            <FormErrors formErrors={this.state.formErrors} />
                        </div>
                        <div className={`form-group ${this.errorClass(this.state.formErrors.email)}`}>
                            <label htmlFor="email">Email address</label>
                            <input type="email" required className="form-control" name="email" placeholder="Email" onChange={this.handleUserInput} />
                        </div>
                        <div className={`form-group ${this.errorClass(this.state.formErrors.password)}`}>
                            <label htmlFor="password">Password</label>
                            <input type="password" className="form-control" name="password" placeholder="Password" onChange={this.handleUserInput} />
                        </div>
                        <button type="submit" className="btn btn-primary" disabled={!this.state.formValid}>Sign In</button>
                    </form>
                </Row>
            </div>
        )
    }
}
export default FormLogin