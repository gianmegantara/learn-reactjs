import React from 'react';

const About = (props) => {
    return (
        <div className='container'>
            <h2 className='m-5 text-center'>This is about page</h2>
            <h2 className='text-center'>this is the parameter {props.match.params.id}</h2>
        </div>
    )
}

export default About;
