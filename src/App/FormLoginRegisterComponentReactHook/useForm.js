import { useState } from 'react'


const useForm = (callback, validate) => {
    const [values, setValues] = useState({ name: '', email: '', password: '', confirmationPassword: '' })
    const [errors, setErrors] = useState({ name: '', email: '', password: '', confirmationPassword: '' })

    const handleUserInput = (event) => {
        const { name, value } = event.target
        setValues({
            ...values,
            [name]: value
        })
    }

    const handleSubmit = event => {
        event.preventDefault()
        setErrors(validate(values))
        //callback()
    }

    return {
        handleUserInput,
        handleSubmit,
        values,
        errors
    }
}

export default useForm

