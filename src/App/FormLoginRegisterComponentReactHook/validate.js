export default function validate(values) {
    let errors = {}

    if (!values.name) {
        errors.name = "name is required"
    } else if (values.name.length < 2) {
        errors.name = "name is too short"
    }

    if (!values.email) {
        errors.email = "email is required"
    } else if (!/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i.test(values.email)) {
        errors.email = "email address is invalid"
    }
    if (!values.password) {
        errors.password = "password is required"
    } else if (values.password.length < 8) {
        errors.password = "password is too short"
    }
    if (values.confirmationPassword !== values.password) {
        errors.confirmationPassword = "password doesnt match"
    }


    return errors
}