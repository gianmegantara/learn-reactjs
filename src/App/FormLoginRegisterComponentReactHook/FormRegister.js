import React from 'react'
import useForm from './useForm'
import validate from './validate'
import { Row } from 'reactstrap'

const FormRegister = () => {
    const { handleUserInput, handleSubmit, values, errors } = useForm(submit, validate)

    function submit() {
        alert('name : ' + values.name + ' email : ' + values.email + ' password :' + values.password)
    }

    return (
        <div className='container'>
            <Row className='m-5'>
                <form className="demoForm" onSubmit={handleSubmit} noValidate>
                    <h2>Sign Up</h2>
                    <div className="form-group">
                        <label>Name</label>
                        <div>
                            <input className="form-control" name='name' type='text' value={values.name} onChange={handleUserInput}></input>
                        </div>
                        {errors.name && <p>{errors.name}</p>}
                    </div>
                    <div className="form-group">
                        <label>Email</label>
                        <div>
                            <input className="form-control" name='email' type='email' value={values.email} onChange={handleUserInput}></input>
                        </div>
                        {errors.email && <p>{errors.email}</p>}
                    </div>
                    <div className="form-group">
                        <label>Password</label>
                        <div>
                            <input className="form-control" name='password' type='password' value={values.password} onChange={handleUserInput}></input>
                        </div>
                        {errors.password && <p>{errors.password}</p>}
                    </div>
                    <div className="form-group">
                        <label>Confirmation Password</label>
                        <div>
                            <input className="form-control" name='confirmationPassword' type='password' value={values.confirmationPassword} onChange={handleUserInput}></input>
                        </div>
                    </div>
                    {errors.confirmationPassword && <p>{errors.confirmationPassword}</p>}
                    <div>
                        <button className="btn btn-primary" type='submit'>Register</button>
                    </div>
                </form>
            </Row>
        </div>
    )
}

export default FormRegister