import React from 'react';
import {
    CardTitle
} from 'reactstrap';

const Username = (props) => {
    return (
        <div>
            <CardTitle>{props.name}</CardTitle>
        </div>
    );
};

export default Username;