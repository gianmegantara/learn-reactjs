import React from 'react';
import {
    CardImg
} from 'reactstrap';

const Avatar = (props) => {
    return (
        <div>
            <CardImg src="https://www.bestfunforall.com/freepik1/imgs/Jobs%20and%20professions%20avatar%20Wallpapers%202.svg" />
        </div>
    );
};

export default Avatar;