import React from 'react';
import Avatar from './Avatar'
import UserName from './UserName'
import Bio from './Bio'
import Clock from './Clock'
import { CardBody, Card } from 'reactstrap'

const UserProfile = () => {
    let names = ['Gian Megantara', 'Ryan Dahl', 'Betram Gilfoyle']
    return names.map((name, index) => (
        <Card className='container col-3 mt-5'>
            <Avatar />
            <CardBody>
                <UserName name={name} />
                <Bio />
                <Clock />
            </CardBody>
        </Card>
    ))
}

export default UserProfile;
