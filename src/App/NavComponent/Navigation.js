import React from 'react'
import {
    Navbar,
    Nav,
    NavItem,
    NavLink
} from 'reactstrap';

const NavBar = () => {
    return (
        <Navbar color="dark" expand="md">
            <Nav>
                <NavItem>
                    <NavLink href="/home">Home</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink href="/register">Register</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink href="/login">Login</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink href="/user">User</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink href="/about">About</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink href="/books">Books</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink href="/globalHook">GlobalHook</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink href="/iseng">Iseng</NavLink>
                </NavItem>
            </Nav>
        </Navbar>
    )
}
export default NavBar

